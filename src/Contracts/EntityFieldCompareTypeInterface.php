<?php

declare(strict_types=1);

namespace Drupal\entity_field_condition\Contracts;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Define the entity field compare type interface.
 */
interface EntityFieldCompareTypeInterface extends ContainerFactoryPluginInterface, ConfigurableInterface, PluginFormInterface {

  /**
   * Evaluate the entity field value.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity instance.
   * @param string $field_name
   *   The entity field name.
   *
   * @return bool
   *   Return TRUE if the field value compare is valid, otherwise FALSE.
   */
  public function evaluate(ContentEntityInterface $entity, string $field_name): bool;

}
