<?php

declare(strict_types=1);

namespace Drupal\entity_field_condition\Contracts;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Define the entity field compare type manager interface.
 */
interface EntityFieldCompareTypeManagerInterface extends PluginManagerInterface {

  /**
   * Get the plugin definition options.
   *
   * @return array
   *   An array of the plugin definition options.
   */
  public function getDefinitionOptions(): array;

}
