<?php

declare(strict_types=1);

namespace Drupal\entity_field_condition\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Define the entity field compare type plugin annotation.
 *
 * @Annotation
 */
class EntityFieldCompareType extends Plugin {

  /**
   * The entity field compare type plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The entity field compare type plugin label.
   *
   * @var string
   */
  public $label;

}
