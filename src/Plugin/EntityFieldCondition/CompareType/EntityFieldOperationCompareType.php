<?php

declare(strict_types=1);

namespace Drupal\entity_field_condition\Plugin\EntityFieldCondition\CompareType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Define the entity field operation compare type plugin.
 *
 * @EntityFieldCompareType(
 *   id = "entity_field_operation_compare",
 *   label = @Translation("Operation")
 * )
 */
class EntityFieldOperationCompareType extends EntityFieldCompareTypePluginBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return [
      'value' => NULL,
      'value_type' => NULL,
      'value_operation' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ): array {

    $form['value_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Value Type'),
      '#required' => TRUE,
      '#options' => [
        'default' => $this->t('Default'),
        'string' => $this->t('String')
      ],
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $this->getValueType(),
    ];
    $form['value_operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Value Operation'),
      '#required' => TRUE,
      '#options' => [
        '==' => $this->t('Equal'),
        '===' => $this->t('Identical'),
        '<' => $this->t('Less Than'),
        '<=' => $this->t('Less Than or Equal to'),
        '>' => $this->t('Greater Than'),
        '>=' => $this->t('Greater Than or Equal to')
      ],
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $this->getValueOperation(),
    ];

    $form['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#required' => TRUE,
      '#default_value' => $this->getValue()
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function evaluate(
    ContentEntityInterface $entity,
    string $field_name
  ): bool {
    if (!$entity->hasField($field_name)) {
      return FALSE;
    }

    return $this->compareValue(
      $this->getEntityFieldValue($entity, $field_name)
    );
  }

  /**
   * Compare the field value based on operation.
   *
   * @param mixed $field_value
   *   The field value.
   *
   * @return bool
   *   Return TRUE if the value is comparable; otherwise FALSE.
   */
  protected function compareValue($field_value): bool {
    $verdict = FALSE;
    $value = $this->getValue();

    switch ($this->getValueOperation()) {
      case '==':
        $verdict = $field_value == $value;
        break;

      case '===':
        $verdict = $field_value === $value;
        break;

      case '<':
        $verdict = $field_value < $value;
        break;

      case '<=':
        $verdict = $field_value <= $value;
        break;

      case '>':
        $verdict = $field_value > $value;
        break;

      case '>=':
        $verdict = $field_value >= $value;
        break;
    }

    return $verdict;
  }

  /**
   * Get entity field value.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   * @param string $field_name
   *   The entity field name.
   *
   * @return mixed|string
   *   The entity field value.
   */
  protected function getEntityFieldValue(
    ContentEntityInterface $entity,
    string $field_name
  ) {
    $field = $entity->get($field_name);
    $value = $field->value;

    if ($this->getValueType() === 'string') {
      $value = $field->getString();
    }

    return $value;
  }

  /**
   * Get the value.
   *
   * @return string
   *   The value type.
   */
  protected function getValue(): ?string {
    return $this->getConfiguration()['value'] ?? NULL;
  }

  /**
   * Get the value type.
   *
   * @return string
   *   The value type.
   */
  protected function getValueType(): string {
    return $this->getConfiguration()['value_type'] ?? 'default';
  }

  /**
   * Get the value operation.
   *
   * @return string|null
   *   The value operation.
   */
  protected function getValueOperation(): ?string {
    return $this->getConfiguration()['value_operation'] ?? NULL;
  }

}
