<?php

declare(strict_types=1);

namespace Drupal\entity_field_condition\Plugin\EntityFieldCondition\CompareType;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\entity_field_condition\Annotation\EntityFieldCompareType;

/**
 * Define the entity field NULL compare type plugin.
 *
 * @EntityFieldCompareType(
 *   id = "entity_field_null_compare",
 *   label = @Translation("IS NULL")
 * )
 */
class EntityFieldNullCompareType extends EntityFieldCompareTypePluginBase {

  /**
   * {@inheritDoc}
   */
  public function evaluate(
    ContentEntityInterface $entity,
    string $field_name
  ): bool {
    if (!$entity->hasField($field_name)) {
      return FALSE;
    }

    return $entity->get($field_name)->value === NULL;
  }

}
