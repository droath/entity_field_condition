<?php

declare(strict_types=1);

namespace Drupal\entity_field_condition\Plugin\Deriver;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the entity field condition deriver.
 */
class EntityFieldConditionDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field condition deriver.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    if (empty($this->derivatives)) {
      foreach ($this->entityTypeManager->getDefinitions() as $type => $definition) {
        if (
          !$definition instanceof ContentEntityTypeInterface
          || !$definition->entityClassImplements(FieldableEntityInterface::class)
        ) {
          continue;
        }
        $this->derivatives[$type] = $base_plugin_definition;
        $this->derivatives[$type]['label'] = $this->t('@entity_type Field', [
          '@entity_type' => $definition->getLabel()
        ]);
        $this->derivatives[$type]['context_definitions'] = [
          'entity' => new EntityContextDefinition("entity:{$type}", $this->t('@entity_type', [
            '@entity_type' => $definition->getLabel(),
          ]))
        ];
      }
    }

    return $this->derivatives;
  }

}
