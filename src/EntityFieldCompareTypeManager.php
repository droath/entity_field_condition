<?php

declare(strict_types=1);

namespace Drupal\entity_field_condition;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\entity_field_condition\Annotation\EntityFieldCompareType;
use Drupal\entity_field_condition\Contracts\EntityFieldCompareTypeInterface;
use Drupal\entity_field_condition\Contracts\EntityFieldCompareTypeManagerInterface;

/**
 * Define the entity field compare type manager.
 */
class EntityFieldCompareTypeManager extends DefaultPluginManager implements EntityFieldCompareTypeManagerInterface {

  /**
   * The entity field compare type manager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/EntityFieldCondition/CompareType',
      $namespaces,
      $module_handler,
      EntityFieldCompareTypeInterface::class,
      EntityFieldCompareType::class
    );

    $this->alterInfo('entity_field_condition_compare_type');
    $this->setCacheBackend($cache_backend, 'entity_field_condition_compare_type');
  }

  /**
   * Get the plugin definition options.
   *
   * @return array
   *   An array of the plugin definition options.
   */
  public function getDefinitionOptions(): array {
    $options = [];

    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if (!isset($definition['label'])) {
        continue;
      }
      $options[$plugin_id] = $definition['label'];
    }

    return $options;
  }

}
